package com.company;

import java.io.IOException;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class   ReadFile {

    private Stack stack = new Stack();
    private Calculator calculator = new Calculator(stack);
    private int size = stack.size();

    Stack ReadFile(String nomeArq) {
        Path path1 = Paths.get(nomeArq);

        try (BufferedReader reader = Files.newBufferedReader(path1, StandardCharsets.UTF_8)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                switch (line) {
                    case "+":
                        calculator.sum();
                        break;

                    case "-":
                        calculator.sub();
                        break;

                    case "/":
                        calculator.div();
                        break;

                    case "*":
                        calculator.mult();
                        break;

                    case "pop":
                        calculator.pop();
                        break;

                    case "dup":
                        calculator.dup();
                        break;

                    case "swap":
                        calculator.swap();
                        break;

                    case "chs":
                        calculator.chs();
                        break;

                    case "sqrt":
                        calculator.sqrt();
                        break;

                    default:
                        stack.push(Double.parseDouble(line));
                        if(size< stack.size()){
                            size = stack.size();
                        }
                }

            }
        }catch (IOException x) {
            System.err.format("Erro de E/S: %s%n", x);
        }
        if(stack.size() >= 2) { throw new IllegalArgumentException("The file doesn't follow the calculator requirements");}
        return stack;
    }

    public int getSize() {
        return size;
    }
}
