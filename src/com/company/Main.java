package com.company;

public class Main {
    public static void main(String[] args) {
        ReadFile aFile = new ReadFile();
        Stack finalResult = aFile.ReadFile("testFile.txt");

        System.out.printf("Final result: %.2f\n", finalResult.top());
        System.out.printf("Maximum Size: %d",aFile.getSize());
    }

}

