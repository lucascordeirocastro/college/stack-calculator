package com.company;

class Stack {

    private  class Node {

        Double element;
        Node prev;


        Node(Double element) {
            this.element = element;
            prev = null;
        }
        public Node(Double element, Node next) {
            this.element = element;
            this.prev = next;
        }
    }

    private int size;
    private Node top;

    Stack() {
        this.top = null;
        this.size = 0;
    }

    void push(Double e) {
        Node temp = new Node(e);
        temp.prev = top;
        top = temp;
        size++;
    }

    Double pop() {
        if (top == null) {
            throw new IllegalArgumentException("Stack underflow");
        }
        Double element = top.element;
        top = top.prev;
        size--;
        return element;
    }

    Double top() {
        if (!isEmpty()) {
            return top.element;
        }
        else {
            throw new IllegalArgumentException("Stack is empty");
        }
    }

    int size() { return this.size; }

    boolean isEmpty()
    {
        return top == null;
    }

    public void clear() {
        if(isEmpty()) {
            throw new IllegalArgumentException("The stack is already empty");
        }

        int originalSize = this.size;

        for(int i=0; i < originalSize; i++) {
            this.pop();
        }
    }

}

