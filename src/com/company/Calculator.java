package com.company;

public class Calculator {
    private Stack stack;
    private ReadFile readFile;

    public Calculator() {
        this.readFile = new ReadFile();
        this.stack = new Stack();

        Stack calculadora = readFile.ReadFile("arquivo.txt");
    }

    public Calculator(Stack parameter) {
        this.stack = parameter;
    }

    void sum() {
        double firstValue = stack.pop();
        double secondValue = stack.pop();

        Double answer = firstValue + secondValue;
        stack.push(answer);
    }

    void div() {
        double firstValue = stack.pop();
        double secondValue = stack.pop();

        if(secondValue == 0){
            throw new ArithmeticException("It's not possible divide by zero");
        }
        else {
            Double answer = firstValue / secondValue;
            stack.push(answer);
        }
    }

    void sub() {
        double firstValue = stack.pop();
        double secondValue = stack.pop();

        Double answer = firstValue - secondValue;
        stack.push(answer);

    }

    void mult() {
        double firstValue = stack.pop();
        double secondValue = stack.pop();

        Double answer = firstValue * secondValue;
        stack.push(answer);

    }

    void pop() {
        if (this.stack.isEmpty()) throw new IllegalArgumentException("Stack underflow");
        this.stack.pop();
    }

    void dup() {
        if (this.stack.isEmpty()) throw new IllegalArgumentException("Stack underflow");
        double duplicate = this.stack.top();
        this.stack.push(duplicate);
    }

    void swap() {
        if (this.stack.isEmpty()) throw new IllegalArgumentException("Stack underflow");
        if(this.stack.size() == 1) return;

        double firstValue = this.stack.pop();
        double secondValue = this.stack.pop();

        this.stack.push(firstValue);
        this.stack.push(secondValue);
    }

    void chs() {
        if (this.stack.isEmpty()) throw new IllegalArgumentException("Stack underflow");

        double value = this.stack.pop();

        if(value >= 0) {
            this.stack.push(value - (value*2));
        } else {
            this.stack.push(value + (value*2));
        }
    }

    void sqrt() {
        if (this.stack.isEmpty()) throw new IllegalArgumentException("Stack underflow");

        double value = this.stack.pop();

        if(value > 0) {
            this.stack.push(Math.sqrt(value));
        } else {
            throw new IllegalArgumentException("You can't square root zero or a negative number");
        }
    }
}